import React from "react";

function Breadcrump(props) {
    return (
        <div>Graditest / Products / {props.name}</div>
    )
}

export default Breadcrump