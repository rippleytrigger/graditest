
import React, { useState } from 'react';
import axios from 'axios';
import Breadcrump from './breadcrump';
import parse from "html-react-parser";
import Modal from "./modal"

class Product extends React.Component { 
    constructor(props){
        super(props)
        this.state = {
            product: {
                images: [],
                options: []
            },
            isLoading: true,
            isOpen: false,
            setIsOpen: false
        }
    }

    componentDidMount(){
        axios
        .get('https://graditest-store.myshopify.com/products/free-trainer-3-mmw.js')
        .then(response => {
            this.setState({product: response.data, isLoading: false});

            console.log(response.data)
        })
        .catch(e => console.log(e))
    }

    convertPriceToDecimals(price){
        let newPrice = price / 100;

        return newPrice.toFixed(2)
    }

    render() {
        const images = this.state.product.images;

        const { isLoading } = this.state;

        const price = this.convertPriceToDecimals(this.state.product.price);
        const maxPrice = this.convertPriceToDecimals(this.state.product.compare_at_price);

        if (isLoading) {
          return (
            <div>
                El Componente está cargando
            </div>
          )
        }

        const colors = this.state.product.options[0].values;
        const sizes = this.state.product.options[1].values;
        let totalprice = price;
        const {isOpen, setIsOpen} = this.state;

        return (
            <div className="product">
                <div className='breadcrump'>
                    <Breadcrump name={this.state.product.title} />
                </div>
                <div className='product-content'>
                    <div className="product-images">
                    <ProductImages images={images} />
                    </div>
                    <div className="product-data">
                        <small>by Nike x ALYX</small>

                        <h1> { this.state.product.title } </h1>

                        <div className='price'>
                            <h2> {'$ ' + price } </h2>
                            <small> { '$ ' + maxPrice } </small>
                        </div>

                        <div className='form'>
                            <form onClick={e => e.preventDefault()}>
                                <fieldset>
                                    <legend>Color</legend>
                                    {
                                        colors.map(color => {
                                            return ( 
                                            <input type="radio" value={color} name="colors" style={{backgroundColor: color}} />
                                            )
                                        })
                                    }
                                </fieldset>
                                <fieldset>
                                    <legend>Size</legend>
                                    {
                                        sizes.map(size => {
                                            return ( 
                                            <div>
                                                {size}
                                            </div>
                                            )
                                        })
                                    }
                                </fieldset>

                                <div>
                                    <button>
                                        -
                                    </button>
                                        1
                                    <button>
                                        +
                                    </button>

                                    <small>Total Price: <b>$ {totalprice}</b></small>
                                </div>
                                <div className='flex'>
                                    <button className='secondary-btn'>
                                        Add to favorite
                                    </button>

                                    <button className='primary-btn' onClick={() => this.setState({ 
                                        isOpen: true,
                                    })}> 
                                        Add to cart
                                    </button>
                                </div>
                            </form>

                            <div>
                                <p className='gray-text'> 
                                    { parse(this.state.product.description) }
                                </p>
                            </div>

                            {isOpen && <Modal setIsOpen={setIsOpen} />}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class ProductImages extends React.Component {
    constructor(props){
        super(props)

        this.slideIndex = 1;
        this.slides = React.createRef()
        this.dots = React.createRef()
        this.state = {
            prevShow: false,
            nextShow: true
        }
    }

    componentDidMount(){
        this.showSlides(this.slideIndex);
    }

    // Next/previous controls
    plusSlides(n) {
        this.showSlides(this.slideIndex += n);
    }

    // Thumbnail image controls
    currentSlide(n) {
        this.showSlides(this.slideIndex = n);
    }

    showSlides(n) {
        let i;
        let slides = this.slides.current.children
        let dots = this.dots.current.children;

        if (n > slides.length) {
            this.setState({
                slideIndex: 1
            })
        }

        if (n < 1) {
            this.setState({
                slideIndex: slides.length
            })
        }

        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }

        for (i = 0; i < dots.length; i++) {
            dots[i].classList.remove("active")
        }

        slides[this.slideIndex -1].style.display =  "block";
        dots[this.slideIndex-1].classList.add("active");

        /* 
            Validar la aparación de las respectivos botones del slider
            según el slide a ver
         */
        this.setState({
            prevShow : (this.slideIndex > 1) ? true : false,
            nextShow: (this.slideIndex < slides.length) ? true : false
        })
    }


    render() {
        const { nextShow, prevShow }= this.state

        console.log(nextShow, prevShow)

        return (

            <div className="slideshow-container">

                <div className='slides' ref={this.slides}>
                {this.props.images.map(
                    (image, index) => {

                    return ( 
                        <div key={index} className="mySlides fade">
                            <img src={image} style={{width:100 + '%'}} />
                        </div>

                    )}
                )}
                </div>


                {/* Botones de desplazamiento de slides */}
                <a style={{display: prevShow ? "block" : "none"}} className="prev" onClick={() => this.plusSlides(-1)}>&#10094;</a>
                <a style={{display: nextShow ? "block" : "none"}} className="next" onClick={() => this.plusSlides(1)}>&#10095;</a>

                <div className='dots' style={{textAlign: "center"}} ref={this.dots}>
                    {this.props.images.map( (image, index) => 
                        <span key={index} className="dot" onClick={() => this.currentSlide(index+1)}></span>
                    )}
                </div>

            </div>
        )
    }
}


export default Product;
