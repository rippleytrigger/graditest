import React from 'react';
import { createRoot } from 'react-dom/client';
import Product from './jsx/product';
import './scss/main.scss'

const container = document.getElementById('root');
const root = createRoot(container);

function App() {
    return ( 
        <Product />
    )
}

root.render(<App />);
